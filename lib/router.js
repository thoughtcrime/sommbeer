Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function() {
    this.route('home', {
        path: '/',
        template: 'home'
    });

    this.route('about',{
      path: 'about',
      template: 'about',
    });

    this.route('team',{
      path: 'team',
      template: 'team',
    });

    this.route('brewers',{
      path: 'brewers',
      template: 'brewers',
    });

    this.route('posts',{
      path: 'posts',
      template: 'posts'
    });

    this.route('admin', {
        path:'/admin',
        template: 'accountsAdmin',
        onBeforeAction: function() {
            if (Meteor.loggingIn()) {
                this.render(this.loadingTemplate);
            } else if(!Roles.userIsInRole(Meteor.user(), ['admin'])) {
                console.log('redirecting');
                this.redirect('/');
            }
        }
    });
});
