Writers = new Mongo.Collection('writers');

if (Meteor.isClient) {
  Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL'
  });
}

if (Meteor.isServer) {
    Meteor.startup(function () {
        // bootstrap the admin user if they exist -- You'll be replacing the id later
        if (Meteor.users.findOne("sommAdmin@example.com"))
            Roles.addUsersToRoles("sommAdmin", ['admin']);

        if(Writers.find().count==0){
          Writers.insert({
            name:'Adam McIntosh',
            handle: '@'+'The1MacAttack',
            handleLink: "https://twitter.com/"+this.handle,
            site: '',
          });

          Writers.insert({
            name:'Andrew Garvin',
            handle: '@'+'abvbeer',
            handleLink: "https://twitter.com/"+this.handle,
            site: 'http://abvconsulting.com',
          });

          Writers.insert({
            name:'Andrew Newton',
            handle: '@'+'DrinkMeLocal',
            handleLink: "https://twitter.com/"+this.handle,
            site: 'http://drinkmelocal.ca',
          });

          Writers.insert({
            name:'Beer and Ballyhoo',
            handle: '@'+'BeerBallyhoo',
            handleLink: "https://twitter.com/"+this.handle,
            site: 'http://beerandballyhoo.tumblr.com',
          });

          Writers.insert({
            name:'Beth Pickhard',
            handle: '@'+'brewcitybiker1',
            handleLink: "https://twitter.com/"+this.handle,
            site: 'http://www.brewcitybiker.com',
          });

        }
    });
    console.log(Writers);

}
